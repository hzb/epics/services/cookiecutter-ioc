# How to use cookiecutter?

```
cookiecutter https://codebase.helmholtz.cloud/hzb/epics/ioc/cookiecutter-ioc.git
```



```
exampleIOC
        iocBoot
                iocexample
        exampleIOCApp
                Db
                src
GenericexampleIOC
        Dockerfile
        modules.yml

```

## Example

```
ioc_name: test
engineer: John Doe
ioc_source_repo: https://codebase.helmholtz.cloud/hzb/epics/ioc/source/testsource.git
ioc_source_tag: main
ubuntu version: "ubuntu_22_04"
epics_version: "7.0.7"
include_autosave: y
autosave_version: R5-10-2
include_seq: y
seq_version: vendor_2_2_8
include_sscan: y
sscan_version: R2-11-5
include_calc: y
calc_version: R3-7-4
include_asyn: y
asyn_version: R4-44-2
include_pcre: y
pcre_version: R8-44
include_stream: y
stream_version: 2.8.24
include_modbus: y
modbus_version: R3-2
include_busy: y
busy_version: R1-7-4
include_ipac: y
ipac_version: 2.16
include_motor: y
motor_version: R7-3-1
include_sim_motor: y
sim_motor_version: v1.2.0

```

